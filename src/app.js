import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import getAppStore from './store/store';
import { addCeleb} from './actions/celebs';
import jsonData from './data.json';


import { filterText, sortBy, clear } from './actions/filters';

import './styles/styles.scss';

import { Provider } from 'react-redux';

const store = getAppStore();

debugger;
jsonData.celebrityList.map(function(celeb){
    store.dispatch(addCeleb({
        name: celeb.name,
        netWorth: celeb.netWorth,
        age: celeb.age,
        country:celeb.country,
        rank:celeb.rank

}));
  })



const template = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);



ReactDOM.render(template, document.getElementById('app'));
