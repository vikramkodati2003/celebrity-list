import uuid from 'uuid';

export const addCeleb = ({   
    name = '',
    netWorth = '',
    age = '',
    country = 0,
    rank=0
} = {}) => ({
    type: 'ADD_CELEB',
    celeb: {
        id: uuid(),
        name,
        netWorth,
        age,
        country,
        rank
    }
});
