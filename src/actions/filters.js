export const filterText = (text = '') => ({
    type: 'FILTER_TEXT',
    text
});

export const filterCountry = (filterCountry= '') => ({
    type: 'FILTER_COUNTRY',
    filterCountry
});

export const sortBy = (sortType='') => ({
    type: 'SORT_BY',
    sortType
});

export const convertCurr = (currency='') => ({
    type: 'CONVERT_CURRENCY',
    currency
});

const filtersReducerDefaultState = {
    text: '',
    sortType: ''
};

export const clear = () => ({
    type: 'CLEAR',
    defaultFilter: filtersReducerDefaultState
});