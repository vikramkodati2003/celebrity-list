// getVisibleCelebs
export default (celebs, {text,filterCountry,sortType,currency}) => {
    return celebs.filter(celeb => {
            const textMatch =
            celeb.name.toLowerCase().includes(text.toLowerCase()) ||
            celeb.country.toLowerCase().includes(text.toLowerCase())||
            celeb.age == text.toLowerCase()||
            celeb.netWorth == text.toLowerCase()||
            celeb.rank == text.toLowerCase();
           
            if(currency&& currency !='Choose currency'){
               celeb.currency=currency;
            }   

            if(filterCountry && filterCountry != 'Choose Country'){
                    const countryMatch =  celeb.country == filterCountry;
                    return textMatch && countryMatch;
            }
    return textMatch
    }).sort((celeb1, celeb2) => {
       
        if (sortType){
            if (sortType === 'Rank') {
            return celeb1.rank<celeb2.rank ? -1:1;            
        } else if (sortType === 'Name') {
            return celeb1.name.localeCompare(celeb2.name);
        }
            else if (sortType === 'Age') {
            return celeb1.age.localeCompare(celeb2.age);
        }
        }
    });
}
