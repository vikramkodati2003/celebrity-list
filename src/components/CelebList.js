import React from 'react';
import { connect } from 'react-redux';
import Celeb from './Celeb';
import getVisibleCelebs from '../selectors/celebs';

const mapStateToProps = (state) => {
    return {
        celebs: getVisibleCelebs(state.celebs, state.filters)
    };
}

const CelebList = (props) => (
    <div class='scrollable'>
        
        <table>
                    <thead>
                      <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>NetWorth   </th>
                        <th>Age </th>
                        <th>Country </th>
                      </tr>
                    </thead>
                    <tbody>
                        {props.celebs.map(celeb => {
                            return (
                                    <Celeb {...celeb} />
                            );
                        })}
                    </tbody>
        </table>
    </div>
);



export default connect(mapStateToProps)(CelebList);