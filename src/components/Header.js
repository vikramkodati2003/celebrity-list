import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
    <header>
        <h1>Celebrity Rich List</h1>
        <h2>A list of the Top 50 Richest Celebrities of 2014</h2>       
        <h4>Reference : http://www.therichest.com/top-lists/top-100-richest-celebrities/</h4>
    </header>
);

export default Header;