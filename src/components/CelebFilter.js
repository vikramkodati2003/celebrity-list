import React from 'react';
import { connect } from 'react-redux';
import { filterText } from '../actions/filters';
import { filterCountry } from '../actions/filters';
import { sortBy } from '../actions/filters';
import { convertCurr } from '../actions/filters';

import countries from '../countries.json';


const sortByList=[{"code":"rank","name":"Rank"},
{"code":"age","name":"Age"},
{"code":"name","name":"Name"}];

const currency =[{"code":"USD","name":"US Dollar"},
{"code":"EUR","name":"Euro"},
{"code":"AUD","name":"Australian Dollar"}];

class CelebFilters extends React.Component {
    constructor(props) {
        super(props);
        //this.filterCountry = this.filterCountry.bind(this);
    }


   
    

    render() {
        let countryList = countries.map((option) =>
                <option id={option.code}>{option.name}</option>
            );

        let currencyList = currency.map((option) =>
            <option id={option.code}>{option.name}</option>
        );

        let orderBy = sortByList.map((option) =>
        <option id={option.code}>{option.name}</option>
            );


        return (
            
            <table>
                <thead>
                    <tr>
                    <th>Filter the results</th></tr>
                </thead>
                <tbody>
                    <tr>
                            <td>
                            Search:  <input type='text' placeholder='search' value={this.props.filters.text}
                                        onChange={(e) => {
                                        this.props.dispatch(filterText(e.target.value));
                            }}></input>
                        
                            </td>
                    </tr>
                    <tr>
                            <td>
                               
                                BirthPlace: 
                                <select   onChange={(e) => {
                                        this.props.dispatch(filterCountry(e.target.value));
                                        }}>
                                         <option id="DEF" selected="selected">Choose Country</option>
                                        {countryList}
                                </select>
                                
                            </td>
                    </tr>
                    <tr>
                            <td>
                                Currency Converter:   
                                <select  onChange={(e) => {
                                   
                                        this.props.dispatch(convertCurr(e.target.value));
                                        }}>
                                        <option id="DEF" selected="selected">Choose currency</option>
                                 {currencyList}
                                </select>
                                
                            </td>
                    </tr>
                    <tr>
                            <td>
                                 Order By:   
                                <select  onChange={(e) => {
                                        this.props.dispatch(sortBy(e.target.value));
                                        }}>>
                                        <option id="DEF" selected="selected">Sort By</option>
                                {orderBy}
                                </select>
                               
                            </td>
                    </tr>
                    </tbody>
                    </table>  
           
        );
    }
}


const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}

export default connect(mapStateToProps)(CelebFilters);

