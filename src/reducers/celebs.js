const celebsReducerDefaultState = [];

export default (state = celebsReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_CELEB':
            return [
                ...state,
                action.celeb
            ];
        default:
            return state;
    }
};