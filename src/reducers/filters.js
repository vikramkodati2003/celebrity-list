const filtersReducerDefaultState = {
    text: '',
    filterCountry:'',
    sortType: ''
};

export default (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case 'FILTER_TEXT':
            return {
                ...state,
                text: action.text
            };

        case 'CONVERT_CURRENCY':
            return {
                ...state,
                currency: action.currency
            };
        case 'SORT_BY':
            return {
                ...state,
                sortType: action.sortType
            };

        case 'FILTER_COUNTRY':
            return {
                ...state,
                filterCountry: action.filterCountry
            };
        case 'CLEAR':
            return {
                ...state,
                text: action.defaultFilter.text,
                sortType: action.defaultFilter.sortType
                
            };
        default:
            return state;
    }
}