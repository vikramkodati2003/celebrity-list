import { createStore, combineReducers } from "redux";
import celebsReducer from '../reducers/celebs';
import filtersReducer from '../reducers/filters';


/*const demoState = {
    celebs: [
        {
            id: '123abcdefghiklmn',
            title: 'Origin',
            description: 'Origin thrusts Robert Langdon into the dangerous intersection of humankind’s two most enduring questions.',
            author: 'Dan Brown',
            published: 2017
        }
    ],
    filters: {
        text: ''
    }
};*/



export default () => {
    return createStore(
        combineReducers({
            celebs: celebsReducer,
            filters: filtersReducer
            
        }
    ));
};